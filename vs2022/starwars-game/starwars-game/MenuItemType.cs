﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace starwars_game
{
    /// <summary>
    ///  Choix du menu
    /// </summary>

    public enum MenuItemType
    {
        Demarrer_Partie = 1,
        Charger_Partie = 2,
        Options = 3,
        Ajouter_Ennemi = 4,
        Quitter = 0
    }
}
